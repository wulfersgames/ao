VERSION 5.00
Begin VB.Form frmOpciones 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   ClientHeight    =   4425
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4485
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOpciones.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmOpciones.frx":0152
   ScaleHeight     =   4425
   ScaleWidth      =   4485
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton asd 
      Caption         =   "Mas Opciones"
      Height          =   255
      Left            =   2280
      TabIndex        =   22
      Top             =   3000
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Fps"
      Height          =   255
      Left            =   720
      TabIndex        =   21
      Top             =   3000
      Width           =   1335
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   3675
      MouseIcon       =   "frmOpciones.frx":F145
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   19
      Top             =   4755
      Width           =   335
   End
   Begin VB.PictureBox PictureSanado 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   1875
      MouseIcon       =   "frmOpciones.frx":F44F
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   7
      Top             =   5520
      Width           =   335
   End
   Begin VB.PictureBox PictureRecuMana 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   360
      MouseIcon       =   "frmOpciones.frx":F759
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   6
      Top             =   5040
      Width           =   335
   End
   Begin VB.PictureBox PictureVestirse 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   360
      MouseIcon       =   "frmOpciones.frx":FA63
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   5
      Top             =   5520
      Width           =   335
   End
   Begin VB.PictureBox PictureMenosCansado 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   1875
      MouseIcon       =   "frmOpciones.frx":FD6D
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   4
      Top             =   5040
      Width           =   335
   End
   Begin VB.PictureBox PictureNoHayNada 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   1875
      MouseIcon       =   "frmOpciones.frx":10077
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   3
      Top             =   4560
      Width           =   335
   End
   Begin VB.PictureBox PictureOcultarse 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   360
      MouseIcon       =   "frmOpciones.frx":10381
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   2
      Top             =   4560
      Width           =   335
   End
   Begin VB.PictureBox PictureFxs 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   2350
      MouseIcon       =   "frmOpciones.frx":1068B
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   1
      Top             =   1080
      Width           =   335
   End
   Begin VB.PictureBox PictureMusica 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   335
      Left            =   840
      MouseIcon       =   "frmOpciones.frx":10995
      MousePointer    =   99  'Custom
      ScaleHeight     =   300
      ScaleWidth      =   300
      TabIndex        =   0
      Top             =   1080
      Width           =   335
   End
   Begin VB.Shape Shape3 
      BorderColor     =   &H00008000&
      Height          =   495
      Left            =   3600
      Top             =   4665
      Width           =   1620
   End
   Begin VB.Label Label12 
      BackColor       =   &H00000000&
      Caption         =   "Modo ventana"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   4035
      TabIndex        =   20
      Top             =   4815
      Width           =   1095
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00008000&
      Height          =   1335
      Left            =   165
      Top             =   4545
      Width           =   3255
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00008000&
      Height          =   375
      Left            =   645
      Top             =   1065
      Width           =   3255
   End
   Begin VB.Label Label11 
      BackColor       =   &H00000000&
      Caption         =   "Manual de F�nixAO"
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   720
      MouseIcon       =   "frmOpciones.frx":10C9F
      MousePointer    =   99  'Custom
      TabIndex        =   18
      Top             =   2520
      Width           =   1455
   End
   Begin VB.Label Label10 
      BackColor       =   &H00000000&
      Caption         =   "Has sanado"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   2235
      TabIndex        =   17
      Top             =   5580
      Width           =   1095
   End
   Begin VB.Label Label9 
      BackColor       =   &H00000000&
      Caption         =   "Meditaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   720
      TabIndex        =   16
      Top             =   5100
      Width           =   975
   End
   Begin VB.Label Label8 
      BackColor       =   &H00000000&
      Caption         =   "No hay nada aqu�"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   315
      Left            =   2235
      TabIndex        =   15
      Top             =   4620
      Width           =   1140
   End
   Begin VB.Label Label7 
      BackColor       =   &H00000000&
      Caption         =   "Abrigarse"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   720
      TabIndex        =   14
      Top             =   5580
      Width           =   975
   End
   Begin VB.Label Label6 
      BackColor       =   &H00000000&
      Caption         =   "Menos cansado"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   2235
      TabIndex        =   13
      Top             =   5115
      Width           =   1095
   End
   Begin VB.Label Label5 
      BackColor       =   &H00000000&
      Caption         =   "Ocultarse"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   720
      TabIndex        =   12
      Top             =   4620
      Width           =   975
   End
   Begin VB.Label Label4 
      BackColor       =   &H00000000&
      Caption         =   "Mostrar carteles"
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   3600
      TabIndex        =   11
      Top             =   5160
      Width           =   1935
   End
   Begin VB.Label Label3 
      BackColor       =   &H00000000&
      Caption         =   "Opciones de sonido"
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   720
      TabIndex        =   10
      Top             =   840
      Width           =   2055
   End
   Begin VB.Label Label2 
      BackColor       =   &H00000000&
      Caption         =   "FXs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   2715
      TabIndex        =   9
      Top             =   1140
      Width           =   375
   End
   Begin VB.Label Label1 
      BackColor       =   &H00000000&
      Caption         =   "M�sica"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   1200
      TabIndex        =   8
      Top             =   1140
      Width           =   615
   End
   Begin VB.Image Image1 
      Height          =   375
      Left            =   3960
      MouseIcon       =   "frmOpciones.frx":10FA9
      MousePointer    =   99  'Custom
      Top             =   0
      Width           =   615
   End
End
Attribute VB_Name = "frmOpciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'F�nixAO 1.0
'
'Based on Argentum Online 0.99z
'Copyright (C) 2002 M�rquez Pablo Ignacio
'
'This program is free software; you can redistribute it and/or modify
'it under the terms of the GNU General Public License as published by
'the Free Software Foundation; either version 2 of the License, or
'any later version.
'
'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.
'
'You should have received a copy of the Affero General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'
'You can contact the original creator of Argentum Online at:
'morgolock@speedy.com.ar
'www.geocities.com/gmorgolock
'Calle 3 n�mero 983 piso 7 dto A
'La Plata - Pcia, Buenos Aires - Republica Argentina
'C�digo Postal 1900
'Pablo Ignacio M�rquez
'
'Argentum Online is based on Baronsoft's VB6 Online RPG
'You can contact the original creator of ORE at aaron@baronsoft.com
'for more information about ORE please visit http://www.baronsoft.com/
'
'You can contact me at:
'elpresi@fenixao.com.ar
'www.fenixao.com.ar
Private Sub Command2_Click()
Me.Visible = False
End Sub

Private Sub asd_Click()
Call frmOpciones2.Show
End Sub

Private Sub command1_Click()
Call MsgBox("Cada vez que presione el boton, los fps ir�n aumentando.", vbInformation, "FPS", 0, 0)
If FPSConfig <> 4 Then
FPSConfig = FPSConfig + 1
Else
FPSConfig = 1
End If
End Sub

Private Sub Form_Load()


Me.Picture = LoadPicture(DirGraficos & "OpcionesDelJuego.jpg")

If Musica = 0 Then
    PictureMusica.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureMusica.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If FX = 0 Then
    PictureFxs.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureFxs.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If NoRes = 1 Then
    Picture1.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    Picture1.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If CartelOcultarse = 1 Then
    PictureOcultarse.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureOcultarse.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If CartelMenosCansado = 1 Then
    PictureMenosCansado.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureMenosCansado.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If CartelVestirse = 1 Then
    PictureVestirse.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureVestirse.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If CartelNoHayNada = 1 Then
    PictureNoHayNada.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureNoHayNada.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If CartelRecuMana = 1 Then
    PictureRecuMana.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureRecuMana.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

If CartelSanado = 1 Then
    PictureSanado.Picture = LoadPicture(DirGraficos & "tick1.jpg")
Else
    PictureSanado.Picture = LoadPicture(DirGraficos & "tick2.jpg")
End If

End Sub
Private Sub Image1_Click()

Me.Visible = False

End Sub
Private Sub Label11_Click()

ShellExecute Me.hWnd, "open", "http://www.fenixao.com.ar/public_html/Html/manual/", "", "", 1

End Sub
Private Sub Picture1_Click()

Dim opcionesDir As String
Set opcionesDir = filePath.GetInitPath("Opciones.opc")

If NoRes = 0 Then
    NoRes = 1
    Picture1.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
    Call WriteVar(opcionesDir, "CONFIG", "ModoVentana", 1)
Else
    NoRes = 0
    Picture1.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
    Call WriteVar(opcionesDir, "CONFIG", "ModoVentana", 0)
End If

MsgBox "Este cambio har� efecto reci�n la pr�xima vez que ejecutes el juego."

End Sub
Private Sub PictureFxs_Click()

Select Case FX
    Case 0
        FX = 1
        PictureFxs.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
    Case 1
        FX = 0
        PictureFxs.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
End Select

Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CONFIG", "FX", Str(FX))

End Sub
Private Sub PictureMenosCansado_Click()

If CartelMenosCansado = 0 Then
    CartelMenosCansado = 1
    PictureMenosCansado.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
Else
    CartelMenosCansado = 0
    PictureMenosCansado.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
End If

Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CARTELES", "MenosCansado", Str(CartelMenosCansado))

End Sub

Private Sub PictureMusica_Click()

Select Case Musica
    Case 0
        Musica = 1
        Audio.StopMidi
        PictureMusica.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
    Case 1
        Musica = 0
        Audio.PlayMIDI CurMidi
        PictureMusica.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
End Select
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CONFIG", "Musica", Str(Musica))

End Sub

Private Sub PictureNoHayNada_Click()
If CartelNoHayNada = 0 Then
    CartelNoHayNada = 1
    PictureNoHayNada.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
Else
    CartelNoHayNada = 0
    PictureNoHayNada.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
End If
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CARTELES", "NoHayNada", Str(CartelNoHayNada))

End Sub

Private Sub PictureOcultarse_Click()

If CartelOcultarse = 0 Then
    CartelOcultarse = 1
    PictureOcultarse.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
Else
    CartelOcultarse = 0
    PictureOcultarse.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
End If
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CARTELES", "Ocultarse", Str(CartelOcultarse))
End Sub

Private Sub PictureRecuMana_Click()
If CartelRecuMana = 0 Then
    CartelRecuMana = 1
    PictureRecuMana.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
Else
    CartelRecuMana = 0
    PictureRecuMana.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
End If
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CARTELES", "RecuMana", Str(CartelRecuMana))

End Sub

Private Sub PictureSanado_Click()
If CartelSanado = 0 Then
    CartelSanado = 1
    PictureSanado.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
Else
    CartelSanado = 0
    PictureSanado.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
End If
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CARTELES", "Sanado", Str(CartelSanado))

End Sub

Private Sub PictureVestirse_Click()
If CartelVestirse = 0 Then
    CartelVestirse = 1
    PictureVestirse.Picture = LoadPicture(filePath.GetGraphicsPath("tick1.jpg"))
Else
    CartelVestirse = 0
    PictureVestirse.Picture = LoadPicture(filePath.GetGraphicsPath("tick2.jpg"))
End If
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CARTELES", "Vestirse", Str(CartelVestirse))

End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

   If bmoving = False And Button = vbLeftButton Then

      Dx3 = X

      dy = y

      bmoving = True

   End If

   

End Sub

 

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)

   If bmoving And ((X <> Dx3) Or (y <> dy)) Then

      Move left + (X - Dx3), top + (y - dy)

   End If

   

End Sub

 

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)

   If Button = vbLeftButton Then

      bmoving = False

   End If

   

End Sub
'FPS
Private Sub FPS1_Click()
FPSConfig = 1
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CONFIG VIDEO", "FPS", "1")
 
End Sub
 
Private Sub FPS2_Click()
FPSConfig = 2
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CONFIG VIDEO", "FPS", "2")
 
End Sub
 
Private Sub FPS3_Click()
FPSConfig = 3
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CONFIG VIDEO", "FPS", "3")
 
End Sub
 
Private Sub FPS4_Click()
FPSConfig = 4
Call WriteVar(filePath.GetInitPath("Opciones.opc"), "CONFIG VIDEO", "FPS", "4")
 
End Sub
'FPS
