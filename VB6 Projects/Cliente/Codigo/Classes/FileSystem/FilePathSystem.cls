VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FilePathSystem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private dataFilePath As String
Private graphicsFilePath As String
Private initFilePath As String
Private mapFilePath As String
Private audioFilePath As String
Private midiFilePath As String
Private screenShotFilePath As String

Public Function Initialize()
    dataFilePath = App.Path + "\Data\"
    graphicsFilePath = dataFilePath + "Graficos\"
    initFilePath = dataFilePath + "Init\"
    mapFilePath = dataFilePath + "Maps\"
    audioFilePath = dataFilePath + "Wav\"
    midiFilePath = dataFilePath + "Midi\"
    screenShotFilePath = dataFilePath + "Screenshots\"
End Function

' This function gets the file path for the graphics
Public Function GetGraphicsPath(filename As String) As String
    GetGraphicsPath = graphicsFilePath & filename
End Function

' This function gets the file path for the Init files
Public Function GetInitPath(filename As String) As String
    GetInitPath = initFilePath & filename
End Function

' This function gets the file path for the Maps
Public Function GetMapPath(filename As String) As String
    GetMapPath = mapFilePath & filename
End Function

' This function gets the file path for the Maps
Public Function GetAudioPath() As String
    GetAudioPath = audioFilePath
End Function

Public Function GetMidiPath() As String
    GetMidiPath = midiFilePath
End Function

Public Function GetScreenShotsPath(filename As String) As String
    GetScreenShotsPath = screenShotFilePath & filename
End Function
