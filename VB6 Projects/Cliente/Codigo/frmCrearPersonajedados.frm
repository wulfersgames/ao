VERSION 5.00
Begin VB.Form frmCrearPersonaje 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8925
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmCrearPersonajedados.frx":0000
   ScaleHeight     =   595
   ScaleMode       =   0  'User
   ScaleWidth      =   12483.01
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCorreo2 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   285
      Left            =   720
      TabIndex        =   31
      ToolTipText     =   "Email"
      Top             =   840
      Width           =   2040
   End
   Begin VB.TextBox txtPasswdCheck 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   720
      PasswordChar    =   "*"
      TabIndex        =   33
      ToolTipText     =   "Contrase�a"
      Top             =   1560
      Width           =   2040
   End
   Begin VB.TextBox txtPasswd 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   720
      PasswordChar    =   "*"
      TabIndex        =   32
      ToolTipText     =   "Contrase�a"
      Top             =   1200
      Width           =   2040
   End
   Begin VB.TextBox txtCorreo 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   285
      Left            =   720
      TabIndex        =   30
      ToolTipText     =   "Email"
      Top             =   600
      Width           =   2040
   End
   Begin VB.ComboBox lstGenero 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      ItemData        =   "frmCrearPersonajedados.frx":25B46
      Left            =   720
      List            =   "frmCrearPersonajedados.frx":25B50
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   37
      Top             =   2280
      Width           =   2040
   End
   Begin VB.ComboBox lstRaza 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      ItemData        =   "frmCrearPersonajedados.frx":25B63
      Left            =   720
      List            =   "frmCrearPersonajedados.frx":25B76
      Sorted          =   -1  'True
      TabIndex        =   35
      Text            =   "lstRaza"
      Top             =   1920
      Width           =   2040
   End
   Begin VB.ComboBox lstHogar 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      ItemData        =   "frmCrearPersonajedados.frx":25BA3
      Left            =   720
      List            =   "frmCrearPersonajedados.frx":25BB6
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   39
      Top             =   2640
      Width           =   2040
   End
   Begin VB.TextBox txtNombre 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   225
      Left            =   4320
      MaxLength       =   20
      TabIndex        =   0
      ToolTipText     =   "Nombre"
      Top             =   600
      Width           =   2415
   End
   Begin VB.Label modCarisma 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   1800
      TabIndex        =   47
      Top             =   4200
      Width           =   690
   End
   Begin VB.Label modInteligencia 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   1800
      TabIndex        =   46
      Top             =   3960
      Width           =   735
   End
   Begin VB.Label modConstitucion 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   1920
      TabIndex        =   45
      Top             =   3720
      Width           =   690
   End
   Begin VB.Label modAgilidad 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   1800
      TabIndex        =   44
      Top             =   3480
      Width           =   735
   End
   Begin VB.Label modfuerza 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   1680
      TabIndex        =   43
      Top             =   3240
      Width           =   855
   End
   Begin VB.Image Image1 
      Height          =   1575
      Left            =   4800
      MouseIcon       =   "frmCrearPersonajedados.frx":25BE7
      MousePointer    =   99  'Custom
      ToolTipText     =   "Tirar dados"
      Top             =   2280
      Width           =   1455
   End
   Begin VB.Label lblPass2OK 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "O"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   24
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   510
      Left            =   2760
      TabIndex        =   42
      Top             =   1080
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label lbSabiduria 
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      ForeColor       =   &H00FFFF00&
      Height          =   255
      Left            =   0
      TabIndex        =   41
      Top             =   0
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "+3"
      ForeColor       =   &H00FFFF80&
      Height          =   195
      Left            =   180
      TabIndex        =   40
      Top             =   0
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Label lblMailOK 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "O"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   24
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   510
      Left            =   2760
      TabIndex        =   38
      Top             =   360
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label lblMail2OK 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "O"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   24
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   510
      Left            =   2760
      TabIndex        =   36
      Top             =   720
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label lblPassOK 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "O"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   24
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   495
      Left            =   2760
      TabIndex        =   34
      Top             =   1440
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   21
      Left            =   11265
      TabIndex        =   29
      Top             =   6480
      Width           =   405
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   42
      Left            =   11640
      MouseIcon       =   "frmCrearPersonajedados.frx":25EF1
      MousePointer    =   99  'Custom
      Top             =   6570
      Width           =   195
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   43
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26043
      MousePointer    =   99  'Custom
      Top             =   6585
      Width           =   195
   End
   Begin VB.Label puntosquedan 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      Caption         =   "32"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   2520
      TabIndex        =   28
      Top             =   4800
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Label Puntos 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      Caption         =   "10"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   8160
      TabIndex        =   27
      Top             =   480
      Width           =   270
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   3
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26195
      MousePointer    =   99  'Custom
      Top             =   465
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   5
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":262E7
      MousePointer    =   99  'Custom
      Top             =   720
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   7
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26439
      MousePointer    =   99  'Custom
      Top             =   1080
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   9
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":2658B
      MousePointer    =   99  'Custom
      Top             =   1350
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   11
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":266DD
      MousePointer    =   99  'Custom
      Top             =   1680
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   13
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":2682F
      MousePointer    =   99  'Custom
      Top             =   1980
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   15
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26981
      MousePointer    =   99  'Custom
      Top             =   2280
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   17
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26AD3
      MousePointer    =   99  'Custom
      Top             =   2550
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   19
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26C25
      MousePointer    =   99  'Custom
      Top             =   2895
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   21
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26D77
      MousePointer    =   99  'Custom
      Top             =   3225
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   23
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":26EC9
      MousePointer    =   99  'Custom
      Top             =   3480
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   25
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":2701B
      MousePointer    =   99  'Custom
      Top             =   3840
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   27
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":2716D
      MousePointer    =   99  'Custom
      Top             =   4095
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   1
      Left            =   11160
      MouseIcon       =   "frmCrearPersonajedados.frx":272BF
      MousePointer    =   99  'Custom
      Top             =   240
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   0
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":27411
      MousePointer    =   99  'Custom
      ToolTipText     =   "Magia"
      Top             =   2640
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   2
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":27563
      MousePointer    =   99  'Custom
      ToolTipText     =   "Robar"
      Top             =   480
      Width           =   435
   End
   Begin VB.Image command1 
      Height          =   255
      Index           =   4
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":276B5
      MousePointer    =   99  'Custom
      ToolTipText     =   "Tacticas de Combate"
      Top             =   1080
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   6
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":27807
      MousePointer    =   99  'Custom
      ToolTipText     =   "Combate con Armas"
      Top             =   1080
      Width           =   420
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   8
      Left            =   8400
      MouseIcon       =   "frmCrearPersonajedados.frx":27959
      MousePointer    =   99  'Custom
      ToolTipText     =   "Meditar"
      Top             =   1560
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   10
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":27AAB
      MousePointer    =   99  'Custom
      ToolTipText     =   "Destreza con dagas"
      Top             =   1560
      Width           =   405
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   12
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":27BFD
      MousePointer    =   99  'Custom
      ToolTipText     =   "Ocultarse"
      Top             =   1560
      Width           =   405
   End
   Begin VB.Image command1 
      Height          =   360
      Index           =   14
      Left            =   9960
      MouseIcon       =   "frmCrearPersonajedados.frx":27D4F
      MousePointer    =   99  'Custom
      ToolTipText     =   "Supervivencia"
      Top             =   1560
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   16
      Left            =   8400
      MouseIcon       =   "frmCrearPersonajedados.frx":27EA1
      MousePointer    =   99  'Custom
      ToolTipText     =   "Talar"
      Top             =   2160
      Width           =   255
   End
   Begin VB.Image command1 
      Height          =   240
      Index           =   18
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":27FF3
      MousePointer    =   99  'Custom
      ToolTipText     =   "Defensa con escudos"
      Top             =   2160
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   255
      Index           =   20
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":28145
      MousePointer    =   99  'Custom
      ToolTipText     =   "Pesca"
      Top             =   2160
      Width           =   405
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   22
      Left            =   8400
      MouseIcon       =   "frmCrearPersonajedados.frx":28297
      MousePointer    =   99  'Custom
      ToolTipText     =   "Mineria"
      Top             =   2640
      Width           =   285
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   24
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":283E9
      MousePointer    =   99  'Custom
      ToolTipText     =   "Carpinteria"
      Top             =   480
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   330
      Index           =   26
      Left            =   9960
      MouseIcon       =   "frmCrearPersonajedados.frx":2853B
      MousePointer    =   99  'Custom
      ToolTipText     =   "Herreria"
      Top             =   2640
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   255
      Index           =   28
      Left            =   8400
      MouseIcon       =   "frmCrearPersonajedados.frx":2868D
      MousePointer    =   99  'Custom
      ToolTipText     =   "Liderazgo"
      Top             =   3240
      Width           =   285
   End
   Begin VB.Image command1 
      Height          =   150
      Index           =   29
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":287DF
      MousePointer    =   99  'Custom
      Top             =   4440
      Width           =   165
   End
   Begin VB.Image command1 
      Height          =   255
      Index           =   30
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":28931
      MousePointer    =   99  'Custom
      ToolTipText     =   "Domar animales"
      Top             =   3240
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   150
      Index           =   31
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":28A83
      MousePointer    =   99  'Custom
      Top             =   4710
      Width           =   150
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   32
      Left            =   8400
      MouseIcon       =   "frmCrearPersonajedados.frx":28BD5
      MousePointer    =   99  'Custom
      ToolTipText     =   "Armas a distancia"
      Top             =   3720
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   135
      Index           =   33
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":28D27
      MousePointer    =   99  'Custom
      Top             =   5040
      Width           =   180
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   34
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":28E79
      MousePointer    =   99  'Custom
      ToolTipText     =   "Combate sin armas"
      Top             =   3720
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   150
      Index           =   35
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":28FCB
      MousePointer    =   99  'Custom
      Top             =   5370
      Width           =   165
   End
   Begin VB.Image command1 
      Height          =   345
      Index           =   36
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":2911D
      MousePointer    =   99  'Custom
      ToolTipText     =   "Navegacion"
      Top             =   3720
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   37
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":2926F
      MousePointer    =   99  'Custom
      Top             =   5640
      Width           =   195
   End
   Begin VB.Image command1 
      Height          =   360
      Index           =   38
      Left            =   9360
      MouseIcon       =   "frmCrearPersonajedados.frx":293C1
      MousePointer    =   99  'Custom
      ToolTipText     =   "Sastreria"
      Top             =   4200
      Width           =   375
   End
   Begin VB.Image command1 
      Height          =   165
      Index           =   39
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":29513
      MousePointer    =   99  'Custom
      Top             =   5985
      Width           =   165
   End
   Begin VB.Image command1 
      Height          =   375
      Index           =   40
      Left            =   8880
      MouseIcon       =   "frmCrearPersonajedados.frx":29665
      MousePointer    =   99  'Custom
      ToolTipText     =   "Comercio"
      Top             =   4800
      Width           =   405
   End
   Begin VB.Image command1 
      Height          =   135
      Index           =   41
      Left            =   11115
      MouseIcon       =   "frmCrearPersonajedados.frx":297B7
      MousePointer    =   99  'Custom
      Top             =   6270
      Width           =   135
   End
   Begin VB.Image boton 
      Height          =   375
      Index           =   1
      Left            =   3240
      MouseIcon       =   "frmCrearPersonajedados.frx":29909
      MousePointer    =   99  'Custom
      Top             =   480
      Width           =   765
   End
   Begin VB.Image boton 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   0
      Left            =   6840
      MouseIcon       =   "frmCrearPersonajedados.frx":29A5B
      MousePointer    =   99  'Custom
      Top             =   480
      Width           =   1200
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   20
      Left            =   11265
      TabIndex        =   26
      Top             =   6180
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   19
      Left            =   11265
      TabIndex        =   25
      Top             =   5880
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   18
      Left            =   11265
      TabIndex        =   24
      Top             =   5565
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   17
      Left            =   11265
      TabIndex        =   23
      Top             =   5250
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   16
      Left            =   11265
      TabIndex        =   22
      Top             =   4965
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   15
      Left            =   11265
      TabIndex        =   21
      Top             =   4665
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   14
      Left            =   11265
      TabIndex        =   20
      Top             =   4350
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   13
      Left            =   11265
      TabIndex        =   19
      Top             =   4050
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   12
      Left            =   11265
      TabIndex        =   18
      Top             =   3750
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   11
      Left            =   11265
      TabIndex        =   17
      Top             =   3435
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   10
      Left            =   11265
      TabIndex        =   16
      Top             =   3120
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   9
      Left            =   11265
      TabIndex        =   15
      Top             =   2820
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   8
      Left            =   11265
      TabIndex        =   14
      Top             =   2505
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   7
      Left            =   11265
      TabIndex        =   13
      Top             =   2205
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   6
      Left            =   11265
      TabIndex        =   12
      Top             =   1890
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   5
      Left            =   11265
      TabIndex        =   11
      Top             =   1590
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   4
      Left            =   11265
      TabIndex        =   10
      Top             =   1290
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   3
      Left            =   11265
      TabIndex        =   9
      Top             =   990
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   2
      Left            =   11265
      TabIndex        =   8
      Top             =   675
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   0
      Left            =   11280
      TabIndex        =   7
      Top             =   120
      Width           =   405
   End
   Begin VB.Label Skill 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   300
      Index           =   1
      Left            =   11265
      TabIndex        =   6
      Top             =   360
      Width           =   405
   End
   Begin VB.Label lbCarisma 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   450
      Left            =   840
      TabIndex        =   5
      ToolTipText     =   "Carisma"
      Top             =   4200
      Width           =   495
   End
   Begin VB.Label lbInteligencia 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   450
      Left            =   840
      TabIndex        =   4
      ToolTipText     =   "Inteligencia"
      Top             =   3960
      Width           =   495
   End
   Begin VB.Label lbConstitucion 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   450
      Left            =   840
      TabIndex        =   3
      ToolTipText     =   "Constitucion"
      Top             =   3720
      Width           =   495
   End
   Begin VB.Label lbAgilidad 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   450
      Left            =   840
      TabIndex        =   2
      ToolTipText     =   "Agilidad"
      Top             =   3480
      Width           =   495
   End
   Begin VB.Label lbFuerza 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   450
      Left            =   840
      TabIndex        =   1
      ToolTipText     =   "Fuerza"
      Top             =   3240
      Width           =   495
   End
End
Attribute VB_Name = "frmCrearPersonaje"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Public SkillPoints As Byte
Function CheckData() As Boolean

If UserRaza = 0 Then
    MsgBox "Seleccione la raza del personaje."
    Exit Function
End If

If UserHogar = 0 Then
    MsgBox "Seleccione el hogar del personaje."
    Exit Function
End If

If UserSexo = -1 Then
    MsgBox "Seleccione el sexo del personaje."
    Exit Function
End If

If SkillPoints > 0 Then
    MsgBox "Asigne los skillpoints del personaje."
    Exit Function
End If

Dim i As Integer
For i = 1 To NUMATRIBUTOS
    If UserAtributos(i) = 0 Then
        MsgBox "Los atributos del personaje son invalidos."
        Exit Function
    End If
Next i

CheckData = True

End Function
Private Sub boton_Click(Index As Integer)
Dim i As Integer
Dim k As Object
        
Call Audio.PlayWave(SND_CLICK)

Select Case Index
    Case 0
        LlegoConfirmacion = False
        Confirmacion = 0

        i = 1
        
        For Each k In Skill
            UserSkills(i) = k.Caption
            i = i + 1
        Next
        
        UserName = txtNombre.Text
        
        If Right$(UserName, 1) = " " Then
            UserName = Trim(UserName)
            MsgBox "Nombre invalido, se han removido los espacios al final del nombre"
        End If
        
        UserRaza = lstRaza.ListIndex + 1
        UserSexo = lstGenero.ListIndex
        UserHogar = lstHogar.ListIndex + 1
        
        UserAtributos(1) = 1
        UserAtributos(2) = 1
        UserAtributos(3) = 1
        UserAtributos(4) = 1
        UserAtributos(5) = 1
        
        If CheckData() Then
            UserPassword = MD5String(txtPasswd.Text)
            UserEmail = txtCorreo.Text
            
            If Not CheckMailString(UserEmail) Then
                MsgBox "Direccion de mail inv�lida.", vbExclamation, "ArkaOn"
                txtCorreo.SetFocus
                Exit Sub
            End If
    
            If UserEmail <> txtCorreo2.Text Then
                MsgBox "Las direcciones de mail no coinciden.", vbExclamation, "ArkaOn"
                txtCorreo2.Text = ""
                txtCorreo2.SetFocus
                Exit Sub
            End If
            
            If Len(Trim(txtPasswd)) = 0 Then
                MsgBox "Ten�s que ingresar una contrase�a.", vbExclamation, "ArkaOn"
                txtPasswd.SetFocus
                Exit Sub
            End If
            
            If Len(Trim(txtPasswd)) < 6 Then
                MsgBox "El password debe tener al menos 6 caracteres.", vbExclamation, "ArkaOn"
                txtPasswd = ""
                txtPasswdCheck = ""
                txtPasswd.SetFocus
                Exit Sub
            End If
            
            If Trim(txtPasswd) <> Trim(txtPasswdCheck) Then
                MsgBox "Las contrase�as no coinciden.", vbInformation, "ArkaOn"
                txtPasswd = ""
                txtPasswdCheck = ""
                txtPasswd.SetFocus
                Exit Sub
            End If
    
            frmMain.Socket1.HostName = IPdelServidor
            frmMain.Socket1.RemotePort = PuertoDelServidor
    
            Me.MousePointer = 11
            EstadoLogin = CrearNuevoPj
    
            If Not frmMain.Socket1.Connected Then
                Call MsgBox("Error: Se ha perdido la conexion con el server.")
                Unload Me
            Else
                Call Login(ValidarLoginMSG(CInt(bRK)))
            End If
            
                CurMidi = "2.mid"
                Call Audio.PlayMIDI(CurMidi)
        
            frmConnect.Picture = LoadPicture(filePath.GetGraphicsPath & "\conectar.jpg")
        End If

    Case 1
            CurMidi = "2.mid"
            Call Audio.PlayMIDI(CurMidi)
        
        frmConnect.Picture = LoadPicture(filePath.GetGraphicsPath & "\conectar.jpg")
        
        frmMain.Socket1.Disconnect
        frmConnect.MousePointer = 1
        Unload Me
End Select

End Sub
Private Sub command1_Click(Index As Integer)
Call Audio.PlayWave(SND_CLICK)

Dim indice
If Index Mod 2 = 0 Then
    If SkillPoints > 0 Then
        indice = Index \ 2
        Skill(indice).Caption = Val(Skill(indice).Caption) + 1
        SkillPoints = SkillPoints - 1
    End If
Else
    If SkillPoints < 10 Then
        
        indice = Index \ 2
        If Val(Skill(indice).Caption) > 0 Then
            Skill(indice).Caption = Val(Skill(indice).Caption) - 1
            SkillPoints = SkillPoints + 1
        End If
    End If
End If

Puntos.Caption = SkillPoints
End Sub
Private Sub Form_Load()

SkillPoints = 10
Puntos.Caption = SkillPoints
Me.Picture = LoadPicture(filePath.GetGraphicsPath("CrearPersonajeConDados.jpg"))
Me.MousePointer = vbDefault

Select Case (lstRaza.List(lstRaza.ListIndex))
    Case Is = "Humano"
        modfuerza.Caption = "+ 1"
        modConstitucion.Caption = "+ 2"
        modAgilidad.Caption = "+ 1"
        modInteligencia.Caption = ""
        modCarisma.Caption = ""
    Case Is = "Elfo"
        modfuerza.Caption = ""
        modConstitucion.Caption = "+ 1"
        modAgilidad.Caption = "+ 3"
        modInteligencia.Caption = "+ 1"
        modCarisma.Caption = "+ 2"
    Case Is = "Elfo Oscuro"
        modfuerza.Caption = "+ 1"
        modConstitucion.Caption = ""
        modAgilidad.Caption = "+ 1"
        modInteligencia.Caption = "+ 2"
        modCarisma.Caption = "- 3"
    Case Is = "Enano"
        modfuerza.Caption = "+ 3"
        modConstitucion.Caption = "+ 3"
        modAgilidad.Caption = "- 1"
        modInteligencia.Caption = "- 6"
        modCarisma.Caption = "- 3"
    Case Is = "Gnomo"
        modfuerza.Caption = "- 5"
        modAgilidad.Caption = "+ 4"
        modInteligencia.Caption = "+ 3"
        modCarisma.Caption = "+ 1"
End Select

End Sub

Private Sub P�cture4_Click()

End Sub

Private Sub Image1_Click()
Audio.PlayWave (SND_CLICK)
Call SendData("TIRDAD")
End Sub

Private Sub lstRaza_click()

Select Case (lstRaza.List(lstRaza.ListIndex))
    Case Is = "Humano"
        modfuerza.Caption = "+ 1"
        modConstitucion.Caption = "+ 2"
        modAgilidad.Caption = "+ 1"
        modInteligencia.Caption = ""
        modCarisma.Caption = ""
    Case Is = "Elfo"
        modfuerza.Caption = ""
        modConstitucion.Caption = "+ 1"
        modAgilidad.Caption = "+ 3"
        modInteligencia.Caption = "+ 1"
        modCarisma.Caption = "+ 2"
    Case Is = "Elfo Oscuro"
        modfuerza.Caption = "+ 1"
        modConstitucion.Caption = ""
        modAgilidad.Caption = "+ 1"
        modInteligencia.Caption = "+ 2"
        modCarisma.Caption = "- 3"
    Case Is = "Enano"
        modfuerza.Caption = "+ 3"
        modConstitucion.Caption = "+ 3"
        modAgilidad.Caption = "- 1"
        modInteligencia.Caption = "- 6"
        modCarisma.Caption = "- 3"
    Case Is = "Gnomo"
        modfuerza.Caption = "- 5"
        modAgilidad.Caption = "+ 4"
        modInteligencia.Caption = "+ 3"
        modCarisma.Caption = "+ 1"
End Select

End Sub

Private Sub txtCorreo_Change()

If Not CheckMailString(txtCorreo) Then
    lblMailOK = "O"
    lblMailOK.ForeColor = &HC0&
    lblMail2OK = "O"
    lblMail2OK.ForeColor = &HC0&
    Exit Sub
End If

lblMailOK = "P"
lblMailOK.ForeColor = &H80FF&

If (UCase$(txtCorreo.Text) = UCase$(txtCorreo2.Text)) Then
    lblMail2OK = "P"
    lblMail2OK.ForeColor = &H80FF&
Else
    lblMail2OK = "O"
    lblMail2OK.ForeColor = &HC0&
End If

End Sub
Private Sub txtCorreo_GotFocus()

MsgBox "La direcci�n de correo electr�nico DEBE SER real."

End Sub
Private Sub txtCorreo2_Change()

If Not CheckMailString(txtCorreo) Then
    lblMailOK = "O"
    lblMailOK.ForeColor = &HC0&
    lblMail2OK = "O"
    lblMail2OK.ForeColor = &HC0&
    Exit Sub
End If

lblMailOK = "P"
lblMailOK.ForeColor = &H80FF&

If (UCase$(txtCorreo.Text) = UCase$(txtCorreo2.Text)) Then
    lblMail2OK = "P"
    lblMail2OK.ForeColor = &H80FF&
Else
    lblMail2OK = "O"
    lblMail2OK.ForeColor = &HC0&
End If

End Sub
Private Sub txtPasswd_Change()

If Len(Trim(txtPasswd)) < 6 Then
    lblPass2OK = "O"
    lblPass2OK.ForeColor = &HC0&
    lblPassOK = "O"
    lblPassOK.ForeColor = &HC0&
    Exit Sub
End If

lblPass2OK = "P"
lblPass2OK.ForeColor = &H80FF&

If (txtPasswdCheck = txtPasswd) Then
    lblPassOK = "P"
    lblPassOK.ForeColor = &H80FF&
Else
    lblPassOK = "O"
    lblPassOK.ForeColor = &HC0&
End If

End Sub
Private Sub txtPasswdCheck_Change()

If Len(Trim(txtPasswd)) < 6 Then
    lblPass2OK = "O"
    lblPass2OK.ForeColor = &HC0&
    lblPassOK = "O"
    lblPassOK.ForeColor = &HC0&
    Exit Sub
End If

lblPass2OK = "P"
lblPass2OK.ForeColor = &H80FF&

If (txtPasswdCheck = txtPasswd) Then
    lblPassOK = "P"
    lblPassOK.ForeColor = &H80FF&
Else
    lblPassOK = "O"
    lblPassOK.ForeColor = &HC0&
End If

End Sub
Private Sub txtNombre_Change()
txtNombre.Text = LTrim(txtNombre.Text)
End Sub

Private Sub txtNombre_GotFocus()
MsgBox "Sea cuidadoso al seleccionar el nombre de su personaje, ArkaniumOnline es un juego de rol, un mundo magico y fantastico, si selecciona un nombre obsceno o con connotaci�n politica los administradores borrar�n su personaje y no habr� ninguna posibilidad de recuperarlo."
End Sub

Private Sub txtNombre_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase$(Chr(KeyAscii)))
End Sub
