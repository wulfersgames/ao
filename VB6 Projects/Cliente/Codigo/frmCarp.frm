VERSION 5.00
Begin VB.Form frmCarp 
   BorderStyle     =   0  'None
   Caption         =   "Carpintero"
   ClientHeight    =   2715
   ClientLeft      =   0
   ClientTop       =   -105
   ClientWidth     =   3240
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   Picture         =   "frmCarp.frx":0000
   ScaleHeight     =   2715
   ScaleWidth      =   3240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCantidad 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000008&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   480
      TabIndex        =   1
      Text            =   "1"
      Top             =   2280
      Width           =   2415
   End
   Begin VB.ListBox lstArmas 
      Appearance      =   0  'Flat
      BackColor       =   &H80000008&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   2175
      ItemData        =   "frmCarp.frx":1EC0
      Left            =   120
      List            =   "frmCarp.frx":1EC2
      MousePointer    =   99  'Custom
      TabIndex        =   0
      Top             =   120
      Width           =   3000
   End
   Begin VB.Image Image2 
      Height          =   375
      Left            =   -360
      MouseIcon       =   "frmCarp.frx":1EC4
      MousePointer    =   99  'Custom
      Top             =   2280
      Width           =   855
   End
   Begin VB.Image Image1 
      Height          =   375
      Left            =   2880
      MouseIcon       =   "frmCarp.frx":21CE
      MousePointer    =   99  'Custom
      Top             =   2280
      Width           =   375
   End
End
Attribute VB_Name = "frmCarp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Command3_Click()
On Error Resume Next

Call SendData("CNC" & ObjCarpintero(lstArmas.ListIndex))

Unload Me
End Sub

Private Sub Command4_Click()
Unload Me
End Sub

Private Sub Form_Deactivate()
Me.SetFocus
End Sub



Private Sub Form_Load()
Me.Picture = LoadPicture(DirGraficos & "fragua-carpinteria.jpg")
End Sub

Private Sub Image1_Click()
On Error Resume Next
Dim stxtCantBuffer As String
stxtCantBuffer = txtCantidad.Text

Call SendData("CNC" & ObjCarpintero(lstArmas.ListIndex) & " " & stxtCantBuffer)
Unload Me

End Sub
Private Sub txtCantidad_Change()

If Val(txtCantidad.Text) < 0 Then
    txtCantidad.Text = 1
End If

If Val(txtCantidad.Text) > MAX_INVENTORY_OBJS Then
    txtCantidad.Text = 1
End If

If Not IsNumeric(txtCantidad.Text) Then txtCantidad.Text = "1"

End Sub
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving = False And Button = vbLeftButton Then
    Dx3 = X
    dy = Y
    bmoving = True
End If

End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving And ((X <> Dx3) Or (Y <> dy)) Then Call Move(left + (X - Dx3), top + (Y - dy))

End Sub
Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbLeftButton Then bmoving = False

End Sub
Private Sub Image2_Click()

Unload Me

End Sub
