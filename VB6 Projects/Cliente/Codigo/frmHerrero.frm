VERSION 5.00
Begin VB.Form frmHerrero 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Herrero"
   ClientHeight    =   3525
   ClientLeft      =   0
   ClientTop       =   360
   ClientWidth     =   3270
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Comic Sans MS"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmHerrero.frx":0000
   ScaleHeight     =   3525
   ScaleWidth      =   3270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstArmas 
      Appearance      =   0  'Flat
      BackColor       =   &H80000007&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000010&
      Height          =   2175
      ItemData        =   "frmHerrero.frx":6E3D
      Left            =   120
      List            =   "frmHerrero.frx":6E3F
      TabIndex        =   1
      Top             =   120
      Width           =   3000
   End
   Begin VB.TextBox txtCantidad 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000007&
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   480
      TabIndex        =   4
      Text            =   "1"
      Top             =   2280
      Width           =   2415
   End
   Begin VB.ListBox lstArmaduras 
      Appearance      =   0  'Flat
      BackColor       =   &H80000007&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   2175
      ItemData        =   "frmHerrero.frx":6E41
      Left            =   120
      List            =   "frmHerrero.frx":6E43
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   3000
   End
   Begin VB.ListBox lstCascos 
      Appearance      =   0  'Flat
      BackColor       =   &H80000007&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   2175
      ItemData        =   "frmHerrero.frx":6E45
      Left            =   120
      List            =   "frmHerrero.frx":6E47
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   2985
   End
   Begin VB.ListBox lstEscudos 
      Appearance      =   0  'Flat
      BackColor       =   &H80000007&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   2175
      ItemData        =   "frmHerrero.frx":6E49
      Left            =   120
      List            =   "frmHerrero.frx":6E4B
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   2985
   End
   Begin VB.Image command7 
      Height          =   615
      Left            =   2520
      MouseIcon       =   "frmHerrero.frx":6E4D
      MousePointer    =   99  'Custom
      Top             =   2760
      Width           =   615
   End
   Begin VB.Image command6 
      Height          =   615
      Left            =   960
      MouseIcon       =   "frmHerrero.frx":7157
      MousePointer    =   99  'Custom
      Top             =   2760
      Width           =   615
   End
   Begin VB.Image command4 
      Height          =   375
      Left            =   120
      MouseIcon       =   "frmHerrero.frx":7461
      MousePointer    =   99  'Custom
      Top             =   2280
      Width           =   375
   End
   Begin VB.Image command3 
      Height          =   375
      Left            =   2880
      MouseIcon       =   "frmHerrero.frx":776B
      MousePointer    =   99  'Custom
      Top             =   2280
      Width           =   375
   End
   Begin VB.Image command2 
      Height          =   615
      Left            =   240
      MouseIcon       =   "frmHerrero.frx":7A75
      MousePointer    =   99  'Custom
      Top             =   2760
      Width           =   615
   End
   Begin VB.Image command1 
      Height          =   615
      Left            =   1680
      MouseIcon       =   "frmHerrero.frx":7D7F
      MousePointer    =   99  'Custom
      Top             =   2760
      Width           =   615
   End
End
Attribute VB_Name = "frmHerrero"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub command1_Click()

lstArmaduras.Visible = False
lstArmas.Visible = True
lstEscudos.Visible = False
lstCascos.Visible = False
End Sub

Private Sub Command2_Click()

lstArmaduras.Visible = True
lstArmas.Visible = False
lstEscudos.Visible = False
lstCascos.Visible = False
End Sub

Private Sub Command3_Click()

On Error Resume Next
Dim stxtCantBuffer As String
stxtCantBuffer = txtCantidad.Text

If lstArmas.Visible Then
 Call SendData("CNS" & ArmasHerrero(lstArmas.ListIndex) & " " & stxtCantBuffer)
ElseIf lstArmaduras.Visible Then
 Call SendData("CNS" & ArmadurasHerrero(lstArmaduras.ListIndex) & " " & stxtCantBuffer)
 ElseIf lstEscudos.Visible Then
 Call SendData("CNS" & EscudosHerrero(lstEscudos.ListIndex) & " " & stxtCantBuffer)
 ElseIf lstCascos.Visible Then
 Call SendData("CNS" & CascosHerrero(lstCascos.ListIndex) & " " & stxtCantBuffer)
End If

Unload Me

End Sub

Private Sub Command4_Click()
Unload Me
End Sub

Private Sub Command6_Click()
lstArmaduras.Visible = False
lstArmas.Visible = False
lstEscudos.Visible = True
lstCascos.Visible = False
End Sub

Private Sub Command7_Click()
lstArmaduras.Visible = False
lstArmas.Visible = False
lstEscudos.Visible = False
lstCascos.Visible = True
End Sub

Private Sub Form_Deactivate()
Me.SetFocus
End Sub
Private Sub Form_Load()

Me.Picture = LoadPicture(DirGraficos & "Herreria.jpg")

End Sub
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving = False And Button = vbLeftButton Then
    Dx3 = X
    dy = Y
    bmoving = True
End If

End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving And ((X <> Dx3) Or (Y <> dy)) Then Move left + (X - Dx3), top + (Y - dy)

End Sub
Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbLeftButton Then bmoving = False

End Sub
Private Sub txtCantidad_Change()

If Not IsNumeric(txtCantidad.Text) Then txtCantidad.Text = "1"

End Sub
