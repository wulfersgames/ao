VERSION 5.00
Begin VB.Form frmComerciar 
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   ClientHeight    =   6135
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6900
   ControlBox      =   0   'False
   ForeColor       =   &H00000000&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmComerciar.frx":0000
   ScaleHeight     =   409
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox precio 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H004DC488&
      Height          =   285
      Left            =   1080
      TabIndex        =   11
      Text            =   "0"
      Top             =   6840
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.TextBox cantidad 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   195
      Left            =   3840
      TabIndex        =   8
      Text            =   "1"
      Top             =   2445
      Width           =   360
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      ForeColor       =   &H00000000&
      Height          =   480
      Left            =   720
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   2
      Top             =   600
      Width           =   480
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   1980
      Index           =   1
      ItemData        =   "frmComerciar.frx":14806
      Left            =   3510
      List            =   "frmComerciar.frx":14808
      TabIndex        =   1
      Top             =   3225
      Width           =   2640
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   1980
      Index           =   0
      ItemData        =   "frmComerciar.frx":1480A
      Left            =   765
      List            =   "frmComerciar.frx":1480C
      TabIndex        =   0
      Top             =   3225
      Width           =   2745
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Index           =   6
      Left            =   3960
      TabIndex        =   12
      Top             =   1680
      Width           =   60
   End
   Begin VB.Image Image2 
      Height          =   210
      Index           =   1
      Left            =   3960
      Top             =   6645
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.Image Image2 
      Height          =   195
      Index           =   0
      Left            =   2760
      Top             =   6720
      Width           =   960
   End
   Begin VB.Image Image1 
      Height          =   405
      Index           =   0
      Left            =   1200
      MouseIcon       =   "frmComerciar.frx":1480E
      MousePointer    =   99  'Custom
      Picture         =   "frmComerciar.frx":14B18
      Tag             =   "1"
      Top             =   2355
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Segoe Print"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   285
      Index           =   5
      Left            =   720
      TabIndex        =   10
      Top             =   1440
      Width           =   60
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Segoe Print"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   285
      Index           =   2
      Left            =   4080
      TabIndex        =   9
      Top             =   600
      Visible         =   0   'False
      Width           =   60
   End
   Begin VB.Label Label2 
      BackColor       =   &H00000000&
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3600
      TabIndex        =   7
      Top             =   6600
      Width           =   975
   End
   Begin VB.Image Command2 
      Height          =   270
      Left            =   6360
      MouseIcon       =   "frmComerciar.frx":15484
      MousePointer    =   99  'Custom
      Tag             =   "1"
      Top             =   0
      Width           =   780
   End
   Begin VB.Image Image1 
      Height          =   405
      Index           =   1
      Left            =   4560
      MouseIcon       =   "frmComerciar.frx":1578E
      MousePointer    =   99  'Custom
      Picture         =   "frmComerciar.frx":15A98
      Tag             =   "1"
      Top             =   2355
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Segoe Print"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   285
      Index           =   3
      Left            =   4080
      TabIndex        =   6
      Top             =   1320
      Visible         =   0   'False
      Width           =   60
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Segoe Print"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   285
      Index           =   4
      Left            =   4080
      TabIndex        =   5
      Top             =   960
      Visible         =   0   'False
      Width           =   60
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Segoe Print"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   285
      Index           =   1
      Left            =   720
      TabIndex        =   4
      Top             =   1800
      Width           =   60
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Segoe Print"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   285
      Index           =   0
      Left            =   720
      TabIndex        =   3
      Top             =   1080
      Width           =   60
   End
End
Attribute VB_Name = "frmComerciar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Public LastIndex1 As Integer
Public LastIndex2 As Integer
Private Sub cantidad_Change()

If Val(cantidad.Text) < 0 Or Val(cantidad.Text) > MAX_INVENTORY_OBJS Then cantidad.Text = 1

End Sub
Private Sub cantidad_KeyPress(KeyAscii As Integer)

If (KeyAscii <> 8) And (KeyAscii <> 6) And (KeyAscii < 48 Or KeyAscii > 57) Then KeyAscii = 0

End Sub
Private Sub Command2_Click()

SendData ("FINCOM")
Call Unload(Me)

End Sub
Private Sub Form_Deactivate()

Me.SetFocus

End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = vbKeyE Then
    If List1(1).ListIndex > -1 And List1(1).ListIndex < MAX_INVENTORY_SLOTS Then
        Call SendData("EQUI" & List1(1).ListIndex + 1)
        Call ActualizarInventario(List1(1).ListIndex + 1)
        Exit Sub
    End If
End If

End Sub

Private Sub Form_Load()

frmComerciar.Picture = LoadPicture(DirGraficos & "\comerciar.jpg")
frmComerciar.Image2(0).Picture = LoadPicture(DirGraficos & "\Cantidad.jpg")
frmComerciar.Image2(1).Picture = LoadPicture(DirGraficos & "\Precio.jpg")
End Sub
Private Sub Image1_Click(Index As Integer)

Call Audio.PlayWave(SND_CLICK)

If List1(Index).List(List1(Index).ListIndex) = "Nada" Or _
   List1(Index).ListIndex < 0 Then
   Picture1.Picture = Nothing
   Exit Sub
End If

Select Case Index
    Case 0
        frmComerciar.List1(0).SetFocus
        LastIndex1 = List1(0).ListIndex
        Select Case Comerciando
            Case 1
                If UserGLD >= OtherInventory(List1(0).ListIndex + 1).Valor * Val(cantidad) Then
                    Call SendData("COMP" & List1(0).ListIndex + 1 & "," & cantidad.Text)
                Else
                    AddtoRichTextBox frmMain.rectxt, "No ten�s suficiente oro.", 2, 51, 223, 1, 1
                    Exit Sub
                End If
            Case 2
                Call SendData("RETI" & List1(0).ListIndex + 1 & "," & cantidad.Text)
            Case 3
                Call SendData("SAVE" & List1(0).ListIndex + 1 & "," & cantidad.Text)
        End Select
        If lista = 1 Then Call ActualizarInformacionComercio(0)
   Case 1
        LastIndex2 = List1(1).ListIndex
        Select Case Comerciando
            Case 1
                If UserInventory(List1(1).ListIndex + 1).Equipped = 0 Then
                    Call SendData("VEND" & List1(1).ListIndex + 1 & "," & cantidad.Text)
                Else
                    AddtoRichTextBox frmMain.rectxt, "No podes vender el item porque lo est�s usando.", 2, 51, 223, 1, 1
                    Exit Sub
                End If
            Case 2
                If UserInventory(List1(1).ListIndex + 1).Equipped = 0 Then
                    Call SendData("DEPO" & List1(1).ListIndex + 1 & "," & cantidad.Text)
                Else
                    AddtoRichTextBox frmMain.rectxt, "No podes depositar el item porque lo est�s usando.", 2, 51, 223, 1, 1
                    Exit Sub
                End If
            Case 3
                If UserInventory(List1(1).ListIndex + 1).Equipped = 0 Then
                    If Val(precio.Text) > 0 Then
                        Call SendData("POVE" & List1(1).ListIndex + 1 & "," & cantidad.Text & "," & precio.Text)
                    Else
                        AddtoRichTextBox frmMain.rectxt, "�Debes elegir un precio de venta!", 2, 51, 223, 1, 1
                        Exit Sub
                    End If
                Else
                    AddtoRichTextBox frmMain.rectxt, "No puedes poner el item a la venta porque lo est�s usando.", 2, 51, 223, 1, 1
                    Exit Sub
                End If

        End Select
        If lista = 0 Then Call ActualizarInformacionComercio(1)
End Select

End Sub

Private Sub list1_Click(Index As Integer)

lista = Index
Call ActualizarInformacionComercio(Index)

End Sub
Private Sub Image1_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

Select Case Index
    Case 0
        If Image1(0).Tag = 1 Then
            Image1(0).Tag = 0
            Image1(1).Tag = 1
        End If
    Case 1
        If Image1(1).Tag = 1 Then
            Image1(1).Tag = 0
            Image1(0).Tag = 1
        End If
End Select

End Sub
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving = False And Button = vbLeftButton Then
   Dx3 = X
   dy = Y
   bmoving = True
End If

End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving And ((X <> Dx3) Or (Y <> dy)) Then Move left + (X - Dx3), top + (Y - dy)

End Sub
Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
If Button = vbLeftButton Then bmoving = False
    
End Sub

