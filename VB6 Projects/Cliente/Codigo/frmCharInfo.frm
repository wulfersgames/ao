VERSION 5.00
Begin VB.Form frmCharInfo 
   BorderStyle     =   0  'None
   Caption         =   "Info"
   ClientHeight    =   5520
   ClientLeft      =   0
   ClientTop       =   -105
   ClientWidth     =   5235
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmCharInfo.frx":0000
   ScaleHeight     =   5520
   ScaleWidth      =   5235
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Label neutrales 
      BackStyle       =   0  'Transparent
      Caption         =   "Neutrales asesinados:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   15
      Top             =   4800
      Width           =   2895
   End
   Begin VB.Image aceptar 
      Height          =   345
      Left            =   3960
      MouseIcon       =   "frmCharInfo.frx":D885
      MousePointer    =   99  'Custom
      Picture         =   "frmCharInfo.frx":DB8F
      Top             =   4320
      Width           =   870
   End
   Begin VB.Image rechazar 
      Height          =   345
      Left            =   3960
      MouseIcon       =   "frmCharInfo.frx":10524
      MousePointer    =   99  'Custom
      Picture         =   "frmCharInfo.frx":1082E
      Top             =   4800
      Width           =   870
   End
   Begin VB.Image desc 
      Height          =   345
      Left            =   3840
      MouseIcon       =   "frmCharInfo.frx":1319D
      MousePointer    =   99  'Custom
      Picture         =   "frmCharInfo.frx":134A7
      Top             =   1680
      Width           =   870
   End
   Begin VB.Image echar 
      Height          =   345
      Left            =   3960
      MouseIcon       =   "frmCharInfo.frx":15DB9
      MousePointer    =   99  'Custom
      Picture         =   "frmCharInfo.frx":160C3
      Top             =   3360
      Width           =   870
   End
   Begin VB.Image Image1 
      Height          =   495
      Left            =   4560
      MouseIcon       =   "frmCharInfo.frx":18A52
      MousePointer    =   99  'Custom
      Top             =   0
      Width           =   735
   End
   Begin VB.Label Ciudadanos 
      BackStyle       =   0  'Transparent
      Caption         =   "Ciudadanos asesinados:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   14
      Top             =   4320
      Width           =   3015
   End
   Begin VB.Label criminales 
      BackStyle       =   0  'Transparent
      Caption         =   "Criminales asesinados:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   13
      Top             =   4560
      Width           =   3135
   End
   Begin VB.Label Solicitudes 
      BackStyle       =   0  'Transparent
      Caption         =   "Solicitudes para ingresar a clanes:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   12
      Top             =   2520
      Width           =   3495
   End
   Begin VB.Label solicitudesRechazadas 
      BackStyle       =   0  'Transparent
      Caption         =   "Solicitudes rechazadas:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   11
      Top             =   2760
      Width           =   3015
   End
   Begin VB.Label fundo 
      BackStyle       =   0  'Transparent
      Caption         =   "Fundo el clan:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   10
      Top             =   3000
      Width           =   3015
   End
   Begin VB.Label lider 
      BackStyle       =   0  'Transparent
      Caption         =   "Veces fue lider de clan:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   9
      Top             =   3240
      Width           =   4695
   End
   Begin VB.Label integro 
      BackStyle       =   0  'Transparent
      Caption         =   "Clanes que integro:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   8
      Top             =   3480
      Width           =   4695
   End
   Begin VB.Label Nombre 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   7
      Top             =   360
      Width           =   4695
   End
   Begin VB.Label Nivel 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Nivel:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   1320
      Width           =   3015
   End
   Begin VB.Label Clase 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Clase:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   5
      Top             =   1080
      Width           =   3135
   End
   Begin VB.Label Raza 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Raza:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   600
      Width           =   4695
   End
   Begin VB.Label Genero 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Genero:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   840
      Width           =   4695
   End
   Begin VB.Label Oro 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Oro:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   1560
      Width           =   4695
   End
   Begin VB.Label Banco 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Banco:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   1
      Top             =   5040
      Width           =   2895
   End
   Begin VB.Label faccion 
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Status:"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   1800
      Width           =   3255
   End
End
Attribute VB_Name = "frmCharInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public frmmiembros As Byte
Public frmsolicitudes As Boolean
Private Sub Aceptar_Click()

frmmiembros = False
frmsolicitudes = False
Call SendData("ACEPTARI" & Right$(Nombre, Len(Nombre) - 8))
frmGuildLeader.Visible = False
Call SendData("GLINFO")

Unload Me

End Sub
Private Sub command1_Click()

Unload Me

End Sub
Public Sub parseCharInfo(ByVal Rdata As String)

Select Case frmmiembros
    Case 0
        rechazar.Visible = True
        aceptar.Visible = True
        echar.Visible = False
        desc.Visible = True
    Case 1
        rechazar.Visible = False
        aceptar.Visible = False
        echar.Visible = True
        desc.Visible = False
    Case 2
        rechazar.Visible = False
        aceptar.Visible = False
        echar.Visible = False
        desc.Visible = False
End Select

Nombre.Caption = "Nombre: " & ReadField(1, Rdata, 44)
Raza.Caption = "Raza: " & ReadField(2, Rdata, 44)
Clase.Caption = "Clase: " & ReadField(3, Rdata, 44)
Genero.Caption = "Genero: " & ReadField(4, Rdata, 44)
Nivel.Caption = "Nivel: " & ReadField(5, Rdata, 44)
Oro.Caption = "Oro: " & ReadField(6, Rdata, 44)
Banco.Caption = "Banco: " & ReadField(7, Rdata, 44)

If Val(ReadField(8, Rdata, 44)) = 1 Then
    fundo.Caption = "Fundo el clan: " & ReadField(9, Rdata, 44)
Else
    fundo.Caption = "Fundo el clan: Ninguno"
End If

Solicitudes.Caption = "Solicitudes para ingresar a clanes: " & ReadField(10, Rdata, 44)
solicitudesRechazadas.Caption = "Solicitudes rechazadas: " & ReadField(11, Rdata, 44)
lider.Caption = "Veces fue lider de clan: " & ReadField(12, Rdata, 44)
integro.Caption = "Clanes que integro: " & ReadField(13, Rdata, 44)

Select Case Val(ReadField(14, Rdata, 44))
    Case 0
        faccion.ForeColor = vbWhite
        faccion.Caption = "Faccion: Neutral"
    Case 1
        faccion.ForeColor = vbBlue
        faccion.Caption = "Faccion: Alianza del Fenix"
    Case 2
        faccion.ForeColor = vbRed
        faccion.Caption = "Faccion: Ej�rcito de Lord Thek"
End Select

neutrales.Caption = "Neutrales asesinados: " & ReadField(15, Rdata, 44)
Ciudadanos.Caption = "Ciudadanos asesinados: " & ReadField(16, Rdata, 44)
criminales.Caption = "Criminales asesinados: " & ReadField(17, Rdata, 44)

Me.Show vbModeless, frmMain

End Sub
Private Sub desc_Click()

Call SendData("ENVCOMEN" & Right$(Nombre, Len(Nombre) - 8))

End Sub
Private Sub Echar_Click()

Call SendData("ECHARCLA" & Right$(Nombre, Len(Nombre) - 8))
frmmiembros = 0
frmsolicitudes = False
frmGuildLeader.Visible = False
Call SendData("GLINFO")
Unload Me

End Sub

Private Sub Form_Load()

Me.Picture = LoadPicture(DirGraficos & "CharInfo.jpg")
echar.Picture = LoadPicture(DirGraficos & "echar.jpg")
desc.Picture = LoadPicture(DirGraficos & "desc.jpg")
aceptar.Picture = LoadPicture(DirGraficos & "aceptar.jpg")
rechazar.Picture = LoadPicture(DirGraficos & "rechazar.jpg")

End Sub

Private Sub Image1_Click()

Unload Me

End Sub

Private Sub Rechazar_Click()

Call SendData("RECHAZAR" & Right$(Nombre, Len(Nombre) - 8))
frmmiembros = 0
frmsolicitudes = False
frmGuildLeader.Visible = False
Call SendData("GLINFO")
Unload Me

End Sub
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving = False And Button = vbLeftButton Then
   Dx3 = X
   dy = Y
   bmoving = True
End If

End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving And ((X <> Dx3) Or (Y <> dy)) Then Move left + (X - Dx3), top + (Y - dy)

End Sub
Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbLeftButton Then bmoving = False

End Sub
Private Sub Text1_Change()

If Val(Text1.Text) < 0 Then
    Text1.Text = MAX_INVENTORY_OBJS
End If

If Val(Text1.Text) > MAX_INVENTORY_OBJS And ItemElegido <> FLAGORO Then
    Text1.Text = 1
End If

End Sub
Private Sub Text1_KeyPress(KeyAscii As Integer)

If (KeyAscii <> 8) Then
    If (Index <> 6) And (KeyAscii < 48 Or KeyAscii > 57) Then
        KeyAscii = 0
    End If
End If

End Sub
