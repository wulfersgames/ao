VERSION 5.00
Begin VB.Form frmSkills3 
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3480
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5235
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmSkills3.frx":0000
   ScaleHeight     =   3480
   ScaleWidth      =   5235
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Image Image1 
      Height          =   255
      Left            =   4680
      MouseIcon       =   "frmSkills3.frx":16D9F
      MousePointer    =   99  'Custom
      Top             =   0
      Width           =   615
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   43
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":170A9
      MousePointer    =   99  'Custom
      Top             =   1320
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   42
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":173B3
      MousePointer    =   99  'Custom
      Top             =   1320
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   22
      Left            =   1800
      TabIndex        =   22
      Top             =   1320
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   1
      Left            =   4320
      TabIndex        =   21
      Top             =   240
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   2
      Left            =   4320
      TabIndex        =   20
      Top             =   600
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   3
      Left            =   4440
      TabIndex        =   19
      Top             =   840
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   18
      Top             =   1080
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   5
      Left            =   4440
      TabIndex        =   17
      Top             =   1440
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   6
      Left            =   4320
      TabIndex        =   16
      Top             =   3000
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   7
      Left            =   4440
      TabIndex        =   15
      Top             =   2760
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   8
      Left            =   4440
      TabIndex        =   14
      Top             =   2520
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   9
      Left            =   4440
      TabIndex        =   13
      Top             =   2160
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   10
      Left            =   4440
      TabIndex        =   12
      Top             =   1920
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   11
      Left            =   4440
      TabIndex        =   11
      Top             =   1680
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   12
      Left            =   1800
      TabIndex        =   10
      Top             =   360
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   0
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":176BD
      MousePointer    =   99  'Custom
      Top             =   240
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   2
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":179C7
      MousePointer    =   99  'Custom
      Top             =   600
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   3
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":17CD1
      MousePointer    =   99  'Custom
      Top             =   600
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   4
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":17FDB
      MousePointer    =   99  'Custom
      Top             =   840
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   5
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":182E5
      MousePointer    =   99  'Custom
      Top             =   840
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   6
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":185EF
      MousePointer    =   99  'Custom
      Top             =   1080
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   7
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":188F9
      MousePointer    =   99  'Custom
      Top             =   1080
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   8
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":18C03
      MousePointer    =   99  'Custom
      Top             =   1320
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   9
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":18F0D
      MousePointer    =   99  'Custom
      Top             =   1320
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   10
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":19217
      MousePointer    =   99  'Custom
      Top             =   3000
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   11
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":19521
      MousePointer    =   99  'Custom
      Top             =   3000
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   12
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":1982B
      Top             =   2640
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   13
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":19B35
      MousePointer    =   99  'Custom
      Top             =   2640
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   14
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":19E3F
      MousePointer    =   99  'Custom
      Top             =   2400
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   15
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":1A149
      MousePointer    =   99  'Custom
      Top             =   2400
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   16
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":1A453
      MousePointer    =   99  'Custom
      Top             =   2160
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   17
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":1A75D
      MousePointer    =   99  'Custom
      Top             =   2160
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   18
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":1AA67
      MousePointer    =   99  'Custom
      Top             =   1920
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   19
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":1AD71
      MousePointer    =   99  'Custom
      Top             =   1920
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   20
      Left            =   4800
      MouseIcon       =   "frmSkills3.frx":1B07B
      MousePointer    =   99  'Custom
      Top             =   1680
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   21
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":1B385
      MousePointer    =   99  'Custom
      Top             =   1680
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   22
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1B68F
      MousePointer    =   99  'Custom
      Top             =   240
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   23
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1B999
      MousePointer    =   99  'Custom
      Top             =   240
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   24
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1BCA3
      MousePointer    =   99  'Custom
      Top             =   480
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   25
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1BFAD
      MousePointer    =   99  'Custom
      Top             =   480
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   13
      Left            =   1800
      TabIndex        =   9
      Top             =   600
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   26
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1C2B7
      MousePointer    =   99  'Custom
      Top             =   840
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   27
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1C5C1
      MousePointer    =   99  'Custom
      Top             =   840
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   14
      Left            =   1800
      TabIndex        =   8
      Top             =   840
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   28
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1C8CB
      MousePointer    =   99  'Custom
      Top             =   1680
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   29
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1CBD5
      MousePointer    =   99  'Custom
      Top             =   1560
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   15
      Left            =   1800
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   30
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1CEDF
      MousePointer    =   99  'Custom
      Top             =   1920
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   180
      Index           =   31
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1D1E9
      MousePointer    =   99  'Custom
      Top             =   1920
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   16
      Left            =   1800
      TabIndex        =   6
      Top             =   1920
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   32
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1D4F3
      MousePointer    =   99  'Custom
      Top             =   2160
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   33
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1D7FD
      MousePointer    =   99  'Custom
      Top             =   2160
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   17
      Left            =   1800
      TabIndex        =   5
      Top             =   2160
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   34
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1DB07
      MousePointer    =   99  'Custom
      Top             =   2400
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   35
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1DE11
      MousePointer    =   99  'Custom
      Top             =   2400
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   18
      Left            =   1800
      TabIndex        =   4
      Top             =   2400
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   1
      Left            =   2640
      MouseIcon       =   "frmSkills3.frx":1E11B
      MousePointer    =   99  'Custom
      Top             =   240
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   19
      Left            =   1800
      TabIndex        =   3
      Top             =   2640
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   36
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1E425
      MousePointer    =   99  'Custom
      Top             =   2640
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   37
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1E72F
      MousePointer    =   99  'Custom
      Top             =   2640
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   20
      Left            =   1800
      TabIndex        =   2
      Top             =   3000
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   38
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1EA39
      MousePointer    =   99  'Custom
      Top             =   3000
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   39
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1ED43
      MousePointer    =   99  'Custom
      Top             =   3000
      Width           =   300
   End
   Begin VB.Label text1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Index           =   21
      Left            =   1800
      TabIndex        =   1
      Top             =   1080
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   40
      Left            =   2280
      MouseIcon       =   "frmSkills3.frx":1F04D
      MousePointer    =   99  'Custom
      Top             =   1080
      Width           =   300
   End
   Begin VB.Image command1 
      Height          =   300
      Index           =   41
      Left            =   240
      MouseIcon       =   "frmSkills3.frx":1F357
      MousePointer    =   99  'Custom
      Top             =   1080
      Width           =   300
   End
   Begin VB.Label puntos 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000016&
      Height          =   195
      Left            =   1935
      TabIndex        =   0
      Top             =   80
      Width           =   105
   End
End
Attribute VB_Name = "frmSkills3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit

Private Sub command1_Click(Index As Integer)

Call Audio.PlayWave(SND_CLICK)

Dim indice
If Index Mod 2 = 0 Then
    If Alocados > 0 Then
        indice = Index \ 2 + 1
        If indice > NUMSKILLS Then indice = NUMSKILLS
        If UserSkills(indice) < MAXSKILLPOINTS And Val(Text1(indice).Caption) < 100 Then
            Text1(indice).Caption = Val(Text1(indice).Caption) + 1
            FLAGS(indice) = FLAGS(indice) + 1
            Alocados = Alocados - 1
        End If
            
    End If
Else
    If Alocados < SkillPoints Then
        
        indice = Index \ 2 + 1
        If Val(Text1(indice).Caption) > 0 And FLAGS(indice) > 0 Then
            Text1(indice).Caption = Val(Text1(indice).Caption) - 1
            FLAGS(indice) = FLAGS(indice) - 1
            Alocados = Alocados + 1
        End If
    End If
End If

Puntos.Caption = Alocados
End Sub

Private Sub Form_Deactivate()

Me.Visible = False
End Sub
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving = False And Button = vbLeftButton Then
    Dx3 = X
    dy = Y
    bmoving = True
End If

End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

If bmoving And ((X <> Dx3) Or (Y <> dy)) Then Move left + (X - Dx3), top + (Y - dy)

End Sub
Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbLeftButton Then bmoving = False

End Sub

Private Sub Form_Load()

Me.Picture = LoadPicture(App.Path & "\Data\Graficos\AgregarPuntosSkills.jpg")




Dim I As Integer

ReDim FLAGS(1 To NUMSKILLS)






End Sub

Private Sub Image1_Click()

Dim I As Integer
Dim cad As String
For I = 1 To NUMSKILLS
    cad = cad & FLAGS(I) & ","
Next
SendData "SKSE" & cad
If Alocados = 0 Then frmMain.Label1.Visible = False
SkillPoints = Alocados
Unload Me
End Sub

