@echo OFF
SET AOInstall="D:\8BS\Projects\AO\Repo\VB6 Projects\Cliente"
cls
echo 	*********************************************
echo 	*********************************************
echo 	*** Unregister prerequisite files for AO  ***
echo 	*********************************************
echo 	*********************************************
echo.
echo IMPORTANT:
echo If you are running Windows Vista or greater, please run this
echo program as an administrator. Right click on this batch file
echo "register.bat" and choose "run as administrator" to be able
echo to properly remove these files from your system.
pause
echo ""
echo "Unregistering RICHTX32.OCX"
regsvr32.exe /u %AOInstall%\RICHTX32.OCX
echo "Unregistering CSWSK32.OCX"
regsvr32.exe /u %AOInstall%\CSWSK32.OCX
echo "Unregistering Msvbvm50.dll"
regsvr32.exe /u %AOInstall%\Msvbvm50.dll
echo "Unregistering msvbvm60.dll"
regsvr32.exe /u %AOInstall%\msvbvm60.dll
echo "Unregistering DX8VB.dll"
regsvr32.exe /u %AOInstall%\DX8VB.DLL
echo "Unregistering Captura.OCX"
regsvr32.exe /u %AOInstall%\Captura.ocx

echo "Unregistering Completed"
pause
