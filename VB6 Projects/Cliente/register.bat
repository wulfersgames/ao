@echo OFF
SET AOInstall="D:\8BS\Projects\AO\Repo\VB6 Projects\Cliente"
cls
echo 	*********************************************
echo 	*********************************************
echo 	*** Registering prerequisite files for AO ***
echo 	*********************************************
echo 	*********************************************
echo.
echo IMPORTANT:
echo If you are running Windows Vista or greater, please run this
echo program as an administrator. Right click on this batch file
echo "register.bat" and choose "run as administrator" to be able
echo to properly install these files on your system.
pause
echo ""
echo "Registering RICHTX32.OCX"
regsvr32.exe %AOInstall%\RICHTX32.OCX
echo "Registering CSWSK32.OCX"
regsvr32.exe %AOInstall%\CSWSK32.OCX
echo "Registering Msvbvm50.dll"
regsvr32.exe %AOInstall%\Msvbvm50.dll
echo "Registering msvbvm60.dll"
regsvr32.exe %AOInstall%\msvbvm60.dll
echo "Registering DX8VB.dll"
regsvr32.exe %AOInstall%\DX8VB.DLL
echo "Registering Captura.OCX"
regsvr32.exe %AOInstall%\Captura.ocx
echo "Registration Completed"
pause
