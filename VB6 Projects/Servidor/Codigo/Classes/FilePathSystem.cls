VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FilePathSystem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private dataPath As String
Private charfilePath As String
Private chrbackupPath As String
Private datPath As String
Private forosPath As String
Private guildsPath As String
Private logsPath As String
Private mapsPath As String
Private worldbackupPath As String

Public Sub Initialize()
    dataPath = App.Path & "\Data\"
    charfilePath = dataPath & "\Charfile\"
    chrbackupPath = dataPath & "\ChrBackUp\"
    datPath = dataPath & "\Dat\"
    forosPath = dataPath & "\Foros\"
    guildsPath = dataPath & "\Guilds\"
    logsPath = dataPath & "\Logs\"
    mapsPath = dataPath & "\Maps\"
    worldbackupPath = dataPath & "\WorldBackUp\"
End Sub

Public Function GetDataPath(filename As String) As String
    GetDataPath = dataPath & filename
End Function

Public Function GetCharFilePath(filename As String) As String
    GetCharFilePath = charfilePath & filename
End Function

Public Function GetChrBackupPath(filename As String) As String
    GetChrBackupPath = chrbackupPath & filename
End Function

Public Function GetDatPath(filename As String) As String
    GetDatPath = datPath & filename
End Function

Public Function GetForosPath(filename As String) As String
    GetForosPath = forosPath & filename
End Function

Public Function GetGuildsPath(filename As String) As String
    GetGuildsPath = guildsPath & filename
End Function

Public Function GetLogsPath(filename As String) As String
    GetLogsPath = logsPath & filename
End Function

Public Function GetMapsPath(filename As String) As String
    GetMapsPath = mapsPath & filename
End Function

Public Function GetWorldBackupPath(filename As String) As String
    GetWorldBackupPath = worldbackupPath & filename
End Function

